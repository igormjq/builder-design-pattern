public class StrogonnoffPizzaBuilder extends PizzaBuilder {

    @Override
    public void buildMassa() {
        pizza.setMassa("Tradicional");
    }

    @Override
    public void buildMolho() {
        pizza.setMolho("Tomate");
    }

    @Override
    public void buildRecheio() {
        pizza.setRecheio("Strogonnoff de Carne, Champignon, Rum");
    }
}

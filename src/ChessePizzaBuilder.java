public class ChessePizzaBuilder extends PizzaBuilder {

    @Override
    public void buildMassa() {
        pizza.setMassa("Italiana");
    }

    @Override
    public void buildMolho() {
        pizza.setMolho("Tomate");
    }

    @Override
    public void buildRecheio() {
        pizza.setRecheio("Mussarela, Gorgonzola, Provolone, Parmesão");
    }
}

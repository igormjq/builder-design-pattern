public class VeganPizzaBuilder extends PizzaBuilder {

    @Override
    public void buildMassa() {
        pizza.setMassa("Integral");
    }

    @Override
    public void buildMolho() {
        pizza.setMolho("Tomate");
    }

    @Override
    public void buildRecheio() {
        pizza.setRecheio("Abobrinha, Manjericão, Tofu, Cebola Roxa");
    }
}

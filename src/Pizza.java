/*
    Product
*/
public class Pizza {

    private String nome;
    private String massa;
    private String molho;
    private String recheio;

    public void setNome(String nome) { this.nome = nome; }

    public void setMassa(String massa) {
        this.massa = massa;
    }

    public void setMolho(String molho) {
        this.molho = molho;
    }

    public void setRecheio(String recheio) {
        this.recheio = recheio;
    }

    @Override
    public String toString() {

        return "Sua pizza foi feita com massa " + massa + ", o delicioso molho " + molho + " e recheada com " + recheio;
    }
}

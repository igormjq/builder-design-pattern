/*
    Abstract Builder
*/
public abstract class PizzaBuilder {

    protected Pizza pizza;

    public Pizza getPizza() {
        return pizza;
    }

    public void createNewPizzaProduct() {
        pizza = new Pizza();
    }

    public abstract void buildMassa();
    public abstract void buildMolho();
    public abstract void buildRecheio();

}

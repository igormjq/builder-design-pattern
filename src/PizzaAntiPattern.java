/*
    Product
*/
public class PizzaAntiPattern {

    private String nome;
    private String massa;
    private String molho;
    private String recheio;

    public PizzaAntiPattern(String nome, String massa, String molho, String recheio) {
        this.nome = nome;
        this.massa = massa;
        this.molho = molho;
        this.recheio = recheio;
    }

    @Override
    public String toString() {

        return "Sua pizza foi feita com massa " + massa + ", o delicioso molho " + molho + " e recheada com " + recheio;
    }
}

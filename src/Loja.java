public class Loja {
    public static void main(String[] args) {

        PizzaAntiPattern pizzaAntiPattern = new PizzaAntiPattern(
                "Quatro queijos",
                "Tradicional",
                "Tomate",
                "Mussarela, Gorgonzola, Provolone, Parmesão"
        );

        Pizzaiolo pizzaiolo = new Pizzaiolo();

        // Polimorfismo
        PizzaBuilder quatroQueijos = new ChessePizzaBuilder();
        PizzaBuilder vegana = new VeganPizzaBuilder();
        PizzaBuilder strogonoff = new StrogonnoffPizzaBuilder();


        // Passa um dos builders para coordenar os passos a serem seguidos.
        pizzaiolo.setPizzaBuilder(vegana);
        // Segue os passos e monta a pizza de acordo com o builder.
        pizzaiolo.constructPizza();
        // Retorna o produto final.
        Pizza pizza = pizzaiolo.getPizza();

        System.out.println("Pizza pronta! " + pizza);


    }
}
